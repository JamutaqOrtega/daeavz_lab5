﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity;
using System.Data.SqlClient;
using System.Data;


namespace Data
{
    public class DProducto
    {
        public List<Producto> Listar(Producto producto)
        {
            SqlParameter[] parameters = null;
            string commandText = string.Empty;
            List<Producto> productos = null;

            try
            {
                commandText = "USP_GetProducto";
                parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@id", SqlDbType.Int);
                parameters[0].Value = producto.IdProducto;
                productos = new List<Producto>();

                using (SqlDataReader reader = SqlHelper.ExecuteReader(SqlHelper.Connection, commandText, CommandType.StoredProcedure, parameters))
                {
                    while (reader.Read())
                    {
                        productos.Add(new Producto
                        {
                            IdProducto = reader["idproducto"] != null ? Convert.ToInt32(reader["idproducto"]) : 0,
                            NombreProducto = reader["nombreProducto"] != null ? Convert.ToString(reader["nombreProducto"]) : string.Empty,
                            CantidadPorUnidad = reader["cantidadPorUnidad"] != null ? Convert.ToString(reader["cantidadPorUnidad"]) : string.Empty,
                            PrecioUnidad = reader["precioUnidad"] != null ? Convert.ToDouble(reader["precioUnidad"]) : 0,
                            CategoriaProducto = reader["categoriaProducto"] != null ? Convert.ToString(reader["categoriaProducto"]) : string.Empty,
                            IdCategoria = reader["idCategoria"] != null ? Convert.ToInt32(reader["idCategoria"]) : 0,
                            IdProveedor = reader["idProveedor"] != null ? Convert.ToInt32(reader["idProveedor"]) : 0,
                            UnidadesEnExistencia = reader["unidadesEnExistencia"] != null ? Convert.ToInt32(reader["unidadesEnExistencia"]) : 0,
                            UnidadesEnPedido = reader["unidadesEnPedido"] != null ? Convert.ToInt32(reader["unidadesEnPedido"]) : 0,
                            NivelNuevoPedido = reader["nivelNuevoPedido"] != null ? Convert.ToInt32(reader["nivelNuevoPedido"]) : 0
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return productos;
        }

        public void Insertar(Producto producto)
        {
            SqlParameter[] parameters = null;
            string commandText = string.Empty;

            try
            {
                commandText = "USP_InstProducto";
                parameters = new SqlParameter[9];
                parameters[0] = new SqlParameter("@nombre", SqlDbType.VarChar);
                parameters[0].Value = producto.NombreProducto;
                parameters[1] = new SqlParameter("@cantidad", SqlDbType.VarChar);
                parameters[1].Value = producto.CantidadPorUnidad;
                parameters[2] = new SqlParameter("@precio_unidad", SqlDbType.Float);
                parameters[2].Value = producto.PrecioUnidad;
                parameters[3] = new SqlParameter("@categoria", SqlDbType.VarChar);
                parameters[3].Value = producto.CategoriaProducto;
                parameters[4] = new SqlParameter("@id_categoria", SqlDbType.Int);
                parameters[4].Value = producto.IdCategoria;
                parameters[5] = new SqlParameter("@id_proveedor", SqlDbType.Int);
                parameters[5].Value = producto.IdProveedor;
                parameters[6] = new SqlParameter("@unidades_existencia", SqlDbType.Int);
                parameters[6].Value = producto.UnidadesEnExistencia;
                parameters[7] = new SqlParameter("@unidades_pedido", SqlDbType.Int);
                parameters[7].Value = producto.UnidadesEnPedido;
                parameters[8] = new SqlParameter("@nivel_nuevo_pedido", SqlDbType.Int);
                parameters[8].Value = producto.NivelNuevoPedido;
                SqlHelper.ExecuteNonQuery(SqlHelper.Connection, commandText, CommandType.StoredProcedure, parameters);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Actualizar(Producto producto)
        {
            SqlParameter[] parameters = null;
            string commandText = string.Empty;

            try
            {
                commandText = "USP_UpdProducto";
                parameters = new SqlParameter[10];
                parameters[0] = new SqlParameter("@id", SqlDbType.Int);
                parameters[0].Value = producto.IdProducto;
                parameters[1] = new SqlParameter("@nombre", SqlDbType.VarChar);
                parameters[1].Value = producto.NombreProducto;
                parameters[2] = new SqlParameter("@cantidad", SqlDbType.VarChar);
                parameters[2].Value = producto.CantidadPorUnidad;
                parameters[3] = new SqlParameter("@precio_unidad", SqlDbType.Float);
                parameters[3].Value = producto.PrecioUnidad;
                parameters[4] = new SqlParameter("@categoria", SqlDbType.VarChar);
                parameters[4].Value = producto.CategoriaProducto;
                parameters[5] = new SqlParameter("@id_categoria", SqlDbType.Int);
                parameters[5].Value = producto.IdCategoria;
                parameters[6] = new SqlParameter("@id_proveedor", SqlDbType.Int);
                parameters[6].Value = producto.IdProveedor;
                parameters[7] = new SqlParameter("@unidades_existencia", SqlDbType.Int);
                parameters[7].Value = producto.UnidadesEnExistencia;
                parameters[8] = new SqlParameter("@unidades_pedido", SqlDbType.Int);
                parameters[8].Value = producto.UnidadesEnPedido;
                parameters[9] = new SqlParameter("@nivel_nuevo_pedido", SqlDbType.Int);
                parameters[9].Value = producto.NivelNuevoPedido;
                SqlHelper.ExecuteNonQuery(SqlHelper.Connection, commandText, CommandType.StoredProcedure, parameters);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Eliminar(int IdProducto, int Suspendido)
        {
            SqlParameter[] parameters = null;
            string commandText = string.Empty;

            try
            {
                commandText = "USP_DelProducto";
                parameters = new SqlParameter[2]; //tamaño del array
                parameters[0] = new SqlParameter("@id", SqlDbType.Int);
                parameters[0].Value = IdProducto;
                parameters[1] = new SqlParameter("@suspendido", SqlDbType.Int);
                parameters[1].Value = Suspendido;
                SqlHelper.ExecuteNonQuery(SqlHelper.Connection, commandText, CommandType.StoredProcedure, parameters);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DAEAvz_lab05
{
    /// <summary>
    /// Interaction logic for Home.xaml
    /// </summary>
    public partial class Home : Window
    {
        public Home()
        {
            InitializeComponent();
        }

        private void btnCategorias_Click(object sender, RoutedEventArgs e)
        {
            ListaCategoria listaCategoria = new ListaCategoria();
            listaCategoria.ShowDialog();
        }

        private void btnProductos_Click(object sender, RoutedEventArgs e)
        {
            ListaProducto listaProducto = new ListaProducto();
            listaProducto.ShowDialog();
        }
    }
}

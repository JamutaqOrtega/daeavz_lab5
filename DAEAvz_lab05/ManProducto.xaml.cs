﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Business;
using Entity;

namespace DAEAvz_lab05
{
    /// <summary>
    /// Interaction logic for ManProducto.xaml
    /// </summary>
    public partial class ManProducto : Window
    {
        public int ID { get; set; }

        public ManProducto(int Id)
        {
            InitializeComponent();
            ID = Id;

            if (ID > 0)
            {
                BProducto bProducto = new BProducto();
                List<Producto> productos = new List<Producto>();
                productos = bProducto.Listar(ID);

                if (productos.Count > 0)
                {
                    lblID.Content = productos[0].IdProducto.ToString();
                    txtNombre.Text = productos[0].NombreProducto;
                    txtCantidad.Text = productos[0].CantidadPorUnidad;
                    txtPrecio.Text = productos[0].PrecioUnidad.ToString();
                    txtCategoria.Text = productos[0].CategoriaProducto;
                    txtIDCategoria.Text = productos[0].IdCategoria.ToString();
                    txtIDProveedor.Text = productos[0].IdProveedor.ToString();
                    txtUnidadesExistencia.Text = productos[0].UnidadesEnExistencia.ToString();
                    txtUnidadesPedido.Text = productos[0].UnidadesEnPedido.ToString();
                    txtNivel.Text = productos[0].NivelNuevoPedido.ToString();
                }

                btnEliminar.IsEnabled = true; //activa el boton
            }
        }

        private void btnGrabar_Click(object sender, RoutedEventArgs e)
        {
            BProducto bProducto = null;
            bool result = true;

            try
            {
                //0: listar todas las categorias
                bProducto = new BProducto();

                if (ID > 0)
                {
                    result = bProducto.Actualizar(new Producto
                    {
                        IdProducto = ID,
                        NombreProducto = txtNombre.Text,
                        CantidadPorUnidad = txtCantidad.Text,
                        PrecioUnidad = Convert.ToDouble(txtPrecio.Text),
                        CategoriaProducto = txtCategoria.Text,
                        IdCategoria = Convert.ToInt32(txtIDCategoria.Text),
                        IdProveedor = Convert.ToInt32(txtIDProveedor.Text),
                        UnidadesEnExistencia = Convert.ToInt32(txtUnidadesExistencia.Text),
                        UnidadesEnPedido = Convert.ToInt32(txtUnidadesPedido.Text),
                        NivelNuevoPedido = Convert.ToInt32(txtNivel.Text)
                    });
                }
                else
                {
                    result = bProducto.Insertar(new Producto
                    {
                        NombreProducto = txtNombre.Text,
                        CantidadPorUnidad = txtCantidad.Text,
                        PrecioUnidad = Convert.ToDouble(txtPrecio.Text),
                        CategoriaProducto = txtCategoria.Text,
                        IdCategoria = Convert.ToInt32(txtIDCategoria.Text),
                        IdProveedor = Convert.ToInt32(txtIDProveedor.Text),
                        UnidadesEnExistencia = Convert.ToInt32(txtUnidadesExistencia.Text),
                        UnidadesEnPedido = Convert.ToInt32(txtUnidadesPedido.Text),
                        NivelNuevoPedido = Convert.ToInt32(txtNivel.Text)
                    });
                }

                if (!result)
                {
                    MessageBox.Show("Comunicarse con el Administrador");
                }

                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Comunicarse con el Administrador -> {ex}");
            }
            finally
            {
                bProducto = null;
            }
        }

        private void btnCerrar_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            BProducto bProducto = null;
            bool result = true;

            try
            {
                //listar todas las categorias
                bProducto = new BProducto();

                //elimina el registro, se pasa 1 para que este como suspendido
                result = bProducto.Eliminar(ID, 1);

                if (!result)
                {
                    MessageBox.Show("Comunicarse con el Administrador");
                }

                Close();
            }
            catch (Exception)
            {
                MessageBox.Show("Comunicarse con el Administrador");
            }
            finally
            {
                bProducto = null;
            }
        }
    }
}
